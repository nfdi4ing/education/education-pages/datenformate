## Inhaltsverzeichnis

1. [Ziele des Trainings](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/2)
2. [Definition Datenformat und Dateiformat](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/3)
3. [Übersicht und Klassifizierung von Datenformaten und Dateiformaten](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/5)
4. [Anforderungen an Datenformate](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/7)
5. [Archetypen-Perspektive auf Datenformate](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/9)
6. [Best Practice, Beispiele](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/datenformate/html_slides/beispielvorlage.html#/10)