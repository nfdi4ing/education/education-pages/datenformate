## 3. Übersicht und Klassifizierung von Datenformaten und Dateiformaten

Daten- und Dateiformate können anhand von unterschiedlichen Aspekten klassifiziert
werden. Während Datenformate anhand von Anwendungsfall und technischem Aspekt
klassifiziert werden können, haben Dateiformate unterschiedliche Eigenschaften, die
bestimmen, wie die Daten verwendet werden können und werden somit anhand von
technischen Eigenschaften klassifiziert.

|                                | Datenformate | Dateiformate | 
|:-------------------------------|:-------------| :------------|
| **Klassifizierung anhand von** | **Anwendungsfall**: z.B. Text, Tabellen, Audio, Bilder   **Technischer Aspekt**: z.B. binär, offen vs. proprietär | **Technischer Aspekt**:   z.B. verlustbehaftet/verlustfrei, komprimiert/unkomprimiert, Verschlüsselung, Grad der Interoperabilität, Erfassung von Metadaten möglich, Sprachagnostik,binär/textbasiert, offen/proprietärz.B. binär, offen vs. proprietär


### Eigenschaften von Dateiformaten

| Eigenschaft | Bedeutung | Beispiel |
|:------------|:----------|:---------|
|**Verlustbehaftet/verlustfrei** | Bei **verlustbehafteten** Dateiformaten wird die Qualität und damit die Dateigröße verringert. Dem entgegen stehen **verlustfreie** Datenformate. | Bilder: JPEG ist ein verlustbehaftetes Format, RAW oder PNG nicht. Wenn also eine RAW-Datei in eine JPEG-Datei konvertiert wird, gehen einige Daten verloren |
|**Komprimiert/unkomprimiert** | **Komprimierung** bedeutet keinen qualitativen Verlust bei kleinerer Dateigröße, kann sich jedoch auf die Verarbeitungsgeschwindigkeit auswirken, da erst einmal dekomprimiert werden muss.| ZIP-Dateien ermöglichen es, mehrere Dateien/Ordner in einer Datei zu speichern. Beim Verpacken der einzelnen Dateien in die ZIP-Datei werden die Dateien in ihrer Speichergröße automatisch verkleinert |
|**Verschlüsselung** | Komplexere Dateiformate erlauben die Verschlüsselung von Inhalten, sodass diese ohne Passwort oder Zertifikat nicht mehr im Klartext lesbar sind. In der Archivierung sind unverschlüsselte Dateiformate ausdrücklich empfohlen.| Dateien können so verschlüsselt werden, dass ein Passwort erforderlich ist, um die Datei zu öffnen. So können z.B. auch Word-Dateien so abgespeichert werden, dass sie ohne Passwort nicht mehr zu öffnen sind. |
|**Grad der Interoperabilität** | Interoperabilität beschreibt die Eigenschaft, mit einem anderen System kooperieren zu können (sowohl Hardware- als auch Software-Bereich).| HTML-Dateien können in verschiedenen Browsern geöffnet werden, während docx-Dateien nur mit Microsoft Word geöffnet werden können (proprietäres Format).|


| Eigenschaft | Bedeutung | Beispiel |
|:------------|:----------|:---------|
|**Erfassung von Metadaten im Dateiformat möglich** | Komplexere Dateiformate erlauben die Erfassung von Metadaten im Datenformat selbst.| Modernere Kameras speichern Metadaten, wie z.B. Aufnahmezeit, Brennweite, Belichtungsdauer, Blende, etc. am Anfang einer Bilddatei im Exif-Format ab.|
|**Sprachagnostik** | Bezieht sich auf Dateiformate, die so verallgemeinert sind, dass das Dateiformat zwischen verschiedenen Systemen/Softwares interoperabel ist.| Das .txt-Dateiformat kann sowohl in Word als auch in Google Sheets geöffnet werden.|
|**Binär / Textbasiert** | Binär / Textbasiert beschreibt die unterschiedliche Kodierung von Daten. Während beides Reihen von Bits sind, beschreibt das textbasierte Dateiformat eine Reihe von 1 und 0 die dem ASCII-Standard entsprechen, wodurch die Textdatei Zeichen repräsentiert. Währenddessen besteht das binäre Dateiformat aus einer für uns unverständlichen Kombination aus 0 und 1, die von Maschinen/Computer interpretiert werden können.| **Binär**: 100 0001 entspricht der „65“ **Textbasiert**: 100 0001 entspricht der „65“ und nach ASCII-Standard dem „A“|
|**Offen/proprietär** | Offene Dateiformate sind in ihrem Aufbau bekannt, sodass Fremdsoftware zum Lesen und Schreiben dieser entwickelt werden kann. Bei proprietären Formaten ist dies hingegen nicht bekannt, sodass hier typischerweise eine Abhängigkeit zu Anbietersoftware besteht.| Tabellen: csv-Dateien sind offene Formate, während .xls (Exceldatei) ein proprietäres Format ist, da es einem Unternehmen gehört, das die Nutzung einschränken kann. |

> *Metadaten sind definiert als Daten, die Informationen über einen oder mehrere Aspekte der Daten enthalten.


### Der schnelle Weg zum Ziel durch die nachfolgenden Übersichten
##### Ablaufplan / Entscheidungsbaum: In drei Schritten zum passenden Datenformat.

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Entscheidungsbaum.png "Title")


| Datenformat | Dateiformat | Beschreibung | Interoperabilität | Beispiel | 
|:------------|:------------|:-------------|:------------------|:---------|
|**Tabellen** | csv | comma separated values | hoch | Messdaten|
||xls, xlsx | Excel-Datei | gering, da proprietäre Software | Messdaten mit Validierungsberechnungen |
|| ods | Open Document Sheet, international genormter Standard | Eher hoch, da genormt | |
|**Datenbanken** | csv |Comma-separated values, Informationen werden durch Kommas getrennt | hoch | Messdaten |
| | xml | eXtensible Markup Language; Sprache, die sowohl von Menschen als auch Maschinen lesbar ist; definiert die Bedeutung von Daten durch Markierungen im Code | hoch | Strukturierte Messdaten |
| | Mdb, accdb | MS Access; Standard-Datenbank-Format von Microsoft | gering, da proprietär | |
|**Text (unformatiert)** | txt | Besteht aus unformatiertem ASCII-Text, kann in jedem Textverarbeitungsprogramm geöffnet und bearbeitet werden | hoch | Notizen |


| Datenformat | Dateiformat | Beschreibung | Interoperabilität | Beispiel | 
|:------------|:------------|:-------------|:------------------|:---------|
| **Text (formatiert)** | xml | eXtensible Markup Language | hoch | |
| | pdf/a | Portable Document Format; kann unabhängig vom ursprünglichen Anwendungsprogramm oder Betriebssystem originalgetreu wiedergegebenen werden | mittel - hoch | Bedienungsanleitungen |
| | html | Hyper Text Markup Language; textbasierte Auszeichnungssprache zur Strukturierung von elektronischen Dokumenten (Webbrowsern) | hoch | |
| | doc, docx | Word-Datei | gering | Dokumentationen, Bedienungsanleitungen |
| | md | Markdown; vereinfachte Auszeichnungssprache, einfacher zu lesen/schreiben als HTML | hoch | |
| | rmd | R markdown: ermöglicht es, R Code in markdown-Dateien einzubetten | eher hoch | |
| | tex | LaTeX; geeignet für die Erstellung technischer und wissenschaftlicher Dokumentationen | | |
| **Code** | abhängig von gewählter Programmiersprache | | | |
| **Anwendungen** | exe | Executable; ausführbare Datei | | |


| Datenformat | Dateiformat | Beschreibung | Interoperabilität | Beispiel | 
|:------------|:------------|:-------------|:------------------|:---------|
| **Container** | tar | Packprogramm, das ermöglicht, Dateien und Verzeichnisse in eine einzige Datei zu schreiben bzw. aus der Datei wiederherzustellen | hoch | |
| | zip | Reduziert bei der verlustfreien Komprimierung den Platzbedarf, ermöglicht es ebenfalls, mehrere Daten zusammenzufassen | hoch | |
| **Bilder** | tiff | Tagged Image File Format, Standardformat für gerasterte Fotos und Druckerdaten | hoch | |
| | jpeg | Entfernt bei der Komprimierung für das menschliche Auge nicht wahrnehmbare Farben und hält die Dateigröße somit möglichst gering | hoch | |
| | pdf | Portable Document Format | hoch | |
| | jp2 | JPEG 2000, ähnlich zum JPEG-Format, hat aber u.a. eine bessere Komprimierungsrate und ermöglicht größere Bilder | hoch | |
| | png | Portable Network Graphic, unterstützt Grafiken mit transparenten oder halbtransparenten Hintergründen | hoch | |
| | gif | Graphics Interchange Format, ermöglicht Kombination von mehreren Bildern zu einfachen Animationen | hoch | 
| | bmp | Bitmap, ermöglicht eine einheitliche Darstellung digitaler Bilder auf unterschiedlichen Geräten und Bildschirmgrößen, werden unkomprimiert gespeichert (größere Dateien) | hoch | |


| Datenformat | Dateiformat | Beschreibung | Interoperabilität | Beispiel | 
|:------------|:------------|:-------------|:------------------|:---------|
| **Video** | mov | eines der am häufigsten verwendeten Videoformate, speichert sowohl Audio als auch Video, Videoeffekte, Untertitel, Text und Bilder in einer einzigen Datei | hoch | |
| | mpeg | Videodatei der Moving Picture Experts Group, kann Daten mit sehr hohen Rate komprimieren, da lediglich die Änderungen zwischen den Frames gespeichert werden und nicht jedes Bild | hoch | |
| | avi | Audio Video Interleave; wird von fast jedem Webbrowser unterstützt, bietet hohe Qualität, daher sind die Dateien groß | hoch | |
| | mxf | Containerformat für Videos, speichert Audio-, Video- und Metadaten, speichert häufig Fernsehwerbung und Filme, die Kinos zur Verfügung gestellt werden sollen | hoch | |
| **Audio** | wave | Verlustfreies Audioformat zur Aufzeichnung (akustischer) Schwingungen | hoch | Maschinengeräusche |
| | aiff | Audio Interchange File Format; von Apple entwickeltes Standard-Audioformat für Macintoshsysteme | hoch | |
| | mp3 | Komprimiert Daten verlustbehaftet, sodass nur die für das menschliche Ohr hörbaren Signale gespeichert werden | hoch | Interview |
| | mxf | s.o. | hoch | |


### Ingenieurspezifische Dateiformate

| Software | Dateiformat | Beschreibung | Interoperabilität | Beispiel | 
|:------------|:------------|:-------------|:------------------|:---------|
| **Allgemein** | bin | Binärdatei; Datei, die beliebige Bitmuster enthalten kann | hoch | |
| | hdf5 | Hierarchical Data Format, wird zur Speicherung von großen Datenmengen verwendet (insb. in wissenschaftlichen Anwendungen) | hoch | |
| **MATLAB** | mat | von MATLAB verwendetes binäres Datencontainerformat, das Arrays, Variablen, Funktionen und andere Datentypen enthalten kann | eher niedrig, da   softwarespezifisch | |
| | Tdms | Technical Data Management Streaming; Enthält Messdaten, die von National Instruments-Software wie LabView aufgezeichnet wurden (inkl. Metadaten wie Verfahren, Testvorrichtung, u.a.) | Eher niedrig, da softwarespezifisch | National Instruments; Software LabView |
| **UNITY** | Fbx | basiert auf Filmbox (FBX); offenes und plattformunabhängiges 3D-Dateiformat, Austauschtechnologie für die Interoperabilität zwischen verschiedenen 3D-Konstruktionssoftwares | hoch | |
| **SOLIDWORKS** | sldprt | Teildokumente (part) | eher gering, da softwarespezifisch | |
| | sldasm | Baugruppendokumente (assembly) | s.o. | |
| | slddrw | Zeichnungsdokumente (drawing ) | s.o. | |
| **AutoCAD** | dwg | Wird für technische Zeichnungen und Designs (2D & 3D) verwendet, enthalten Vektorgrafiken und Metadaten | gering, da proprietär | |
| | dxf | Drawing Interchange File Format; ermöglicht den CAD-Datenaustausch, einfache Datenstruktur, somit auf vielen Betriebssystemen verwendbar | eher hoch, quasi-standard
