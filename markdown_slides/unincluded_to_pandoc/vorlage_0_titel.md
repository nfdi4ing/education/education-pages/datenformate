## Datenformate
<!-- ÄNDERN SIE IN DIESER DATEI NUR IHREN TITEL, DO NOT CHANGE BELOW!-->
<style>

.speaking-bubble {
  position: absolute;
  top: 440%; 
  left: 50%;
  width: 200px;
  height: 115px;
  margin: 0 auto;
  margin-bottom: 30px;
  background-color: #909599;
  color: black;
  border-radius: 10px;
  text-align: center;
  font-size: 20px;
}

.speaking-bubble::after {
  content: "";
  position: absolute;
  bottom: -8px;
  left: 15%;
  transform: translateX(-50%);
  border-style: solid;
  border-width: 10px 10px 0 10px;
  border-color: #909599 transparent transparent transparent;
}

.speaking-bubble2 {
  position: absolute;
  top: 150%;
  left: 101%;
  width: 200px;
  height: 115px;
  margin: 0 auto;
  margin-bottom: 30px;
  background-color: #909599;
  color: black;
  border-radius: 10px;
  text-align: center;
  font-size: 20px;
}

.speaking-bubble2::after {
  content: "";
  position: absolute;
  bottom: -8px;
  left: 85%;
  transform: translateX(-50%);
  border-style: solid;
  border-width: 10px 10px 0 10px;
  border-color: #909599 transparent transparent transparent;
}
</style> 

<div class="speaking-bubble">
  <p>Scrolle runter um <b>die nächste Folie</b> eines Kapitels zu erkunden!</p>
</div>
<div class="speaking-bubble2">
  <p>Scrolle rechts um <b>das nächste Kapitel</b> zu erkunden!</p>
</div>

<!-- #### PDF Download 
Hier lässt sich das [Trainingsskript herunterladen](url) -->

Zu diesem Training [bestehendes **Feedback ansehen**](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/issues) bzw. [neues **Feedback geben**](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/issues/new).


## Hinweise

* Navigieren Sie mit den Pfeiltasten oder dem Pfeilsymbolen. Scrollen Sie nach rechts zum nächsten Kapitel und nach unten für die Inhalte des jeweiligen Kapitels. 

![Navigation Arrows](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/navigation_arrows.png)

* Auf den letzten Folien finden Sie Kontakthinweise, Lizenzhinweise und eine Druckfunktion zum Export der Folien als PDF. 
* Erstellen Sie für Feedback ein Issue in unserer Startseite


## Shortcuts Tastatur 

* `[S]`: Speaker-View (Ansicht für Vortragenden) in Pop-Up-Fenster öffnen 
* `[F11]`: Vollbildmodus öffnen bzw. verlassen 
* `[ESC]`: Übersicht aller Folien 
* `[B]` oder `[.]`: Blackout, d.h. Folien zu schwarz blenden (bspw. damit diese nicht ablenken)
* Pfeiltasten :arrow_up_small: :arrow_down_small: :arrow_forward: :arrow_backward: : Zur vorherigen bzw. nächsten Folie blättern
<!-- * `[ALT]` + `Mausklick` (Windows) bzw. `[STRG]` + `Mausklick` (Linux): An Folie heranzoomen bzw. herauszoomen -->
* `[Pos1]`: Zur ersten Folie (Titelfolie) springen, `[End]`: Zur letzten Folie (Abschlussfolie) springen

