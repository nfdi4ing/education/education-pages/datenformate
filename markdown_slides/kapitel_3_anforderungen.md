## 4. Anforderungen an Datenformate

#### Einführung

Neben den Eigenschaften, die Daten- und Dateiformate mitbringen, stellen sich auch Anforderungen an Daten- und Datenformate. Der Umgang mit Forschungsdaten soll den FAIR-Prinzipien (Findable, Accessible, Interoperable, Reusable) entsprechen.
Die Anforderungen an Datenformate, die sich daraus ergeben, sind:
* Interoperabilität
* Speicherbarkeit
* Flexibilität von Datenstrukturen


#### Interoperabilität

* Interoperabilität beschreibt die Eigenschaft, mit einem anderen System kooperieren zu können.
* Die Daten sollten so vorliegen, dass sie ausgetauscht, interpretiert und mit anderen Datensätzen von Menschen sowie Computersystemen kombiniert werden können.
* Interoperabilität bedeutet auch, dass die gleichen Daten in unterschiedlichen Datenformaten darstellbar sind.
    * Ein Beispiel ist ein einfacher Text: Dieser kann in einem Artikel oder einem Buch dargestellt werden. Der Text kann aber auch auf einem Bild abgebildet oder in einem Video gezeigt werden. Die gleichen Daten (Text) können also in unterschiedlichen Formaten (Buch, Bild, Video) abgebildet werden.


#### Speicherbarkeit

* Da Forschungsdaten langfristig gespeichert werden sollen (i.d.R. für zehn Jahre), sollten die Daten in offenen Dateiformaten abgespeichert werden, damit der Zugriff und die Interpretierbarkeit über den Speicherzeitraum gewährleistet ist.

* Es sollten also keine proprietären Dateiformate gewählt werden, da sich die Software bspw. über den Zeitraum verändern kann und die Daten somit beim Öffnen nicht mehr verlustfrei wieerhergestellt werden können.

* Z.B. sollte einer Textdatei eher in einer txt.-Datei gespeichert werden und nicht in einer Word-Datei, da Microsoft regelmäßig neue Versionen von Word veröffentlicht und so bspw. die Formatierung verloren gehen kann.


#### Flexibilität von Datenstrukturen

Datenstrukturen dienen dazu, Informationen und Daten strukturiert zu organisieren und zu verwalten, um ein effizientes Zugreifen auf die Daten zu ermöglichen. Die Daten werden dabei miteinander verknüpft.
Es werden statische und dynamische Datenstrukturen unterschieden

|      | **Statische Datenstrukturen** | **Dynamische Datenstrukturen** |
|:-----|:------------------------------|:-------------------------------|
| **Eigenschaften/Anpassung der Größe** | Die Größe der Datenstruktur muss vorher bekannt sein. Soll die Größe später neu angepasst werden, muss die gesamte Datenstruktur neu erstellt werden. | Dynamische Datenstrukturen sind während der Laufzeit in ihrer Speichergröße veränderbar |
| **Beispiele** | integer- und boolean-Datentypen, Arrays | Listen, Queues, Stacks |

