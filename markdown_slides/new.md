# Datenbanken

## Dateien können in Datenbanken (DB) gespeichert werden

* DB sind elektronische Archive, die die strukturierte Speicherung von großen Mengen von Dateien und Daten ermöglichen
* Dabei gibt es unterschiedliche Arten von Datenbanken, die sich in ihrer Form und Stuktur unterscheiden (relational, zB.)


## Weiteres Wissen

* Die häufigsten Form sind relationale Datenbanken, bei denen die Datensätze in Tabellen gespeichert werden



# Datenmanagagementpläne in Datenbanken

## Inhalt

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/beispielformate.jpg "Unser Beispielbild")
