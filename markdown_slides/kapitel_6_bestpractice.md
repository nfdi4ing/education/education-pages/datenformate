## 6. Best-Practice und ingenieurwissenschaftliche Beispiele 
#### Windowstricks: Dateiformat darstellen

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Windowstricks_Dateiformat_darstellen.png" alt="" width="1642" height="769"> 


#### Windowstricks: Standard-Apps auswählen

In den Windows-Einstellungen lässt sich festlegen, mit
welchem Programm bspw. CSV-Dateien
standardmäßig geöffnet werden sollen (hier z.Z. Excel
2016, alternativ bspw. Texteditor).

Windows-Taste > Standard-Apps eintippen > Enter

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/StandardApps_nach_Dateityp_auswählen.png" alt="" width="1069" height="323">


#### Text-basierte Dateiformate im Texteditor oder Notepad++ öffnen

Nicht-binäre und Text-basierte Dateien lassen sich typischerweise auch in Texteditoren (Microsoft Texteditor, Notepad++) öffnen. Dies bietet die Chance, auch in unbekannte Dateiformate hineinzuschauen.

Vorsicht bei größeren Dateien, ab einigen Megabyte kann das Öffnen bereits einige Zeit in Anspruch nehmen.
Binäre Dateien hingegen erzeugen einen nicht lesbaren technischen Kauderwelsch.

> Disclaimer: Vorsicht beim Öffnen von Daten aus fremden Quellen!


#### Richtiger Umgang mit CSV

Das Trennzeichen - z.B. das Komma „,“ – sollte … 

*... nicht als Dezimaltrennzeichen für Zahlen genutzt werden
*... nicht in Texten vorkommen, bzw. die Texte als solche mit Anführugszeichen gekennzeichnet werden, sodass Kommata in Texten nicht als Trennzeichen interpretiert werden

Teilweise wird das Trennzeichen länderspezifisch interpretiert (z.B. Excel tut dies, wenn eine CSV darin geöffnet wird). Dies gilt es auch zu beachten, wenn Daten von Sensoren oder Maschinen ausgelesen werden. 

|               | Trennzeichen | Dezimalzeichen |
|:--------------|:-------------|:---------------|
| International | , (Komma) | . (Punkt) |
| DE            | ; (Semikolon) | , (Komma) |


#### Auswahl der Dateiformate
* Als allgemeine Faustregel gilt, dass Sie bei der aktiven Arbeit mit einem Datensatz das Dateiformat verwenden sollten, das Ihrer Arbeitsweise am besten entspricht. In den meisten Fällen wird dies von der Software diktiert, die Sie verwenden. Wenn Sie über eine gewisse Flexibilität verfügen, etwa weil Ihre Software mehrere Formate unterstützt oder Sie Ihre eigene Software schreiben, sollten Sie ein archivtaugliches Format verwenden (wie unten beschrieben)

* Wenn Sie die Arbeit mit einem bestimmten Datensatz beendet haben, sollten Sie ihn in ein stabileres Standardformat für die Archivierung umwandeln. Es kommt immer häufiger vor, dass alte Dateien nicht mehr lesbar sind, nur weil die Software, mit der sie erstellt wurden, nicht mehr verfügbar ist

* Idealerweise sollte Ihr Archivierungsformat mindestens mit einem der folgenden kostenlosen Tools lesbar sein (idealerweise als reiner Text): Damit der Zugriff ohne eine möglicherweise teure Lizenz möglich ist
* ein gut dokumentierter Standard: Damit eine breite Palette von Software zur Verfügung steht, um auf das Format zuzugreifen
* ein De-facto-Standard in Ihrem Forschungsgebiet: Damit die Mehrheit der Forscher, mit denen Sie das Dokument austauschen, auch Zugang zu der richtigen Software hat
* Versuchen Sie, wenn möglich, ein Format zu wählen, dass es Ihnen erlaubt, die Daten direkt in der Datei zu beschreiben und zu dokumentieren (Metadaten).


#### Ingenieurwissenschaftliche Beispiele: Der Flug der Ariane 5

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Beispiel_Ariane5.png "Ariane 5")

[Zum Video](https://www.youtube.com/watch?v=gp_D8r-2hwk)

[https://www.golem.de/news/softwarefehler-in-der-raumfahrt-in-den-neunzigern-stuerzte-alles-ab-1511-117537.html](https://www.golem.de/news/softwarefehler-in-der-raumfahrt-in-den-neunzigern-stuerzte-alles-ab-1511-117537.html)


#### Ingenieurwissenschaftliche Beispiele: Airbus A380 - Kabelbäume

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Beispiel_A380.png "Kabelbäume")

[https://www.grin.com/document/143609](https://www.grin.com/document/143609)


#### Anekdoten aus anderen Fachbereichen: Öffnen von Datenformaten in Excel 

Für tabellenbasierte Dateiformate (xls und xlsx, aber auch csv) bietet Microsoft Excel teilweise an, diese ebenfalls zu öffnen. Wie Daten dabei unbewusst geändert werden können, zeigen diese beiden Anekdoten:

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Ankuendigung_Covid_und_Gene.png "Virus und Gene")


#### Beispiel 3: Fehler bei COVID-Meldungen in England

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Beispiel_Covid.png "Covid")

[https://www.bbc.com/news/technology-54423988](https://www.bbc.com/news/technology-54423988)


#### Beispiel 4: Wie Excel Gennamen automatisch als Datumsangabe formatierte
![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Beispiel_Gene.png "Gene")

[https://www.sueddeutsche.de/digital/microsoft-excel-genforschung-namen-1.4992440](https://www.sueddeutsche.de/digital/microsoft-excel-genforschung-namen-1.4992440)


#### Weitere Beispiele

[https://forschungsdaten-thueringen.de/fdm-scarytales/articles/ueberblick.html](https://forschungsdaten-thueringen.de/fdm-scarytales/articles/ueberblick.html)

