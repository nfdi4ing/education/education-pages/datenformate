## 5. Ingenieurwissenschaftliche Perspektive der [NFDI4Ing Archetypen](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#/) auf Datenformate

#### Welche Datenformate nutzen die Archetypen?

> [![Archetyp Alex speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Alex_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#alex) `CSV` für Zeitreihen, `JPEG`/`JPG` für Kamerabilder, allgemein `HDF5`

> [![Archetyp Doris speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Doris_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#doris) `HDF5`, `ASCII`, `CGNS`, eigene binäre Formate, Software, Bilddateien, Punktwolken etc.

> [![Archetyp Frank speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Frank_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#frank) `CSV` für strukturierte Daten wie Sensordaten oder auch Umfrageergebnisse; Grafik-, Video- und Audioformate für Produkte und Produktionsmachinen


#### Vor welchen Herausforderungen stehen die Archetypen mit ihren Datenformaten?

> [![Archetyp Alex speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Alex_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#alex) Große Datenmengen, von denen sich nicht alle in HDF5 speichern lassen

> [![Archetyp Doris speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Doris_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#doris) Große und vor allem immobile Daten mit geringer Interoperabilität

> [![Archetyp Frank speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Frank_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#frank) Proprietäre Datenformate von Maschinenherstellern erschweren das Auslesen und Verarbeiten von Sensordaten 


#### Wie hat Forschungsdatenmanagement den Umgang mit Datentypen verändert?

> [![Archetyp Alex speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Alex_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#alex) Metadaten haben an Bedeutung gewonnen, daher HDF5 verwendet, um Metadaten benutzerfreundlich zusammen mit Daten abzuspeichern

> [![Archetyp Doris speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Doris_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#doris) Das Bewusstsein für Interoperabilität hat sich gewandelt: Weniger 'exotische' Formate, fokussierte Maschinenlesbarkeit, Metadaten 

> [![Archetyp Frank speaking](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/raw/master/media_files/Speaking_Frank_veryverysmall.png)](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html#frank) Es ist ein Unterschied, ob man Daten nur für sich erhebt, oder auch Nachnutzende bedenkt: Interoperable Dateiformate, Beschreibung in Metadaten
