## 1. Ziele des Trainings

**Zu vermittelndes Wissen und Inhalte**

[<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/datenformate_lzm.png" alt="Lernzielmatrix für Daten, Datentypen, Datenformate">](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/datenformate_lzm.png)


Quelle für Lernzielmatix zum Thema FDM: 
[Petersen, Britta, Engelhardt, Claudia, Hörner, Tanja, Jacob, Juliane, Kvetnaya, Tatiana, Mühlichen, Andreas, Schranzhofer, Hermann, Schulz, Sandra, Slowig, Benjamin, Trautwein-Bruns, Ute, Voigt, Anne, & Wiljes, Cord. (2022). Lernzielmatrix zum Themenbereich Forschungsdatenmanagement (FDM) für die Zielgruppen Studierende, PhDs und Data Stewards (Version 1). Zenodo. [https://doi.org/10.5281/zenodo.7034478](https://doi.org/10.5281/zenodo.7034478)]



## 2. Definiton Datenformate und Dateiformate

### Datenformat

* Das Datenformat legt die Struktur sowie die Interpretation von Daten fest
* Datenformate beschreiben das Format von einzelnen Datenfeldern (Bsp. Tabelle)
    * Spalten repräsentieren die "Kategorie"
    * Zeilen repräsentieren Datensätze, die jede Spalte mit ihrem entsprechenden Wert füllen

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Tabelle.png" alt="" width="1174" height="233"> 


**Datenformate werden außerdem durch ihre Syntax und Semantik beschrieben.**

#### Syntax
* Lehre vom Satzbau
* Eindeutige Verständnisweise, die durch die Zeichenfolge beschrieben wird
* In der Programmiersprache umfasst die Syntax Befehle und Zeichen

#### Semantik
* Teilgebiet der Linguistik
Bedeutung/Inhalt einer Information
* Programmiersprache: Semantik beinhaltet logische und algebraische Elemente


### Dateiformat

Dateiformate sind Standardverfahren zur Speicherung von Daten auf einem Computer und
legen fest, wie Bits zur Codierung von Informationen in einem digitalen Speichermedium
verwendet werden
* Bei digitalen Dateien ist die Kenntnis des Dateiformats essentiell,
damit die darin enthaltenen Daten von einem Computer erkannt und interpretiert werden
können. Die Endung des Dateinamens definiert das Dateiformat (z.B. abcxyz.pdf).

* Datei- und Datenformate begrenzen sich nicht nur auf digitale Dateien. Ein Buch ist
beispielsweise auch ein Dateiformat. Die Texte im Buch sind dann Datenformate

* Ein weiteres Beispiel ist eine Exceltabelle: Diese wird häufig als .xlsx-Datei exportiert
– dies ist zunächst das Dateiformat. Die Inhalte der Tabelle sind Datenformate (z.B.
Zeichen, Gleitkommazahlen, etc.)


### Datenbanken

Dateien können in **Datenbanken** (DB) gespeichert werden

* DB sind elektronische Archive, die die strukturierte
Speicherung von großen Mengen von Dateien und
Daten ermöglichen.

* Dabei gibt es unterschiedliche Arten von Datenbanken,
die sich in ihrer Form und Struktur unterscheiden (z.B.
relational / Graphen-DB / Dokumenten-DB, strukturiert /
semi-strukturiert).

> Die häufigste Form sind relationale Datenbanken, bei denen die Datensätze in Tabellen gespeichert werden.

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/datenformate/-/raw/master/media_files/Bild_DB.png" alt="" width="904" height="417"> 
